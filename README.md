HOSTS
=====

SPLITS [**@STEVENBLACK**'S HOSTS](https://github.com/StevenBlack/hosts) SOCIAL EXTENSION INTO ONE FILE PER SOCIAL NETWORK.

**@STEVENBLACK**'S HOSTS SOCIAL EXTENSION USES [**@SINFONIETTA**'s HOSTFILES](https://github.com/Sinfonietta/hostfiles).
